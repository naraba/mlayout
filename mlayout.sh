#!/bin/bash

# TODO: handle csv-format input file, now only for fixed length

if [ $# -ne 2 ]; then
  echo "usage: ${0} [multiple-layout fixed length file] [config file]"
  exit -1
elif [ ! -e $1 ]; then
  echo "input file ${1} not found."
  exit -1
elif [ ! -e $2 ]; then
  echo "config file ${2} not found."
  exit -1
fi

i_file=$1
c_file=$2

# config hash table with segment-key to parent-key
declare -A CONFIG_ARY


# load config file and make hash table
while read line
do
  c_key=`echo ${line} | cut -d ',' -f 1`
  c_value=`echo ${line} | cut -d ',' -f 2`
  CONFIG_ARY[${c_key}]=${c_value}
done < ${c_file}


# length of segment-key
keylen=${#c_key}

# dummy root segment-key
root_seg=`printf "%0${keylen}d" 0 | tr '0' '-'`

# input file name
iname="${i_file##*/}"

# input file path
ipath="${i_file%/*}"


# remove all segment files if exist
rm ${ipath}/${iname}_*


# load input file and make each segment file
# each line in file forms [parent segment-id][parent segment-key][segment-id][original segment line]
# [segment-id][segment-key] is composite key to oneself (segment-key is the first field of the original line)
# [parent segment-id][parent segment-key] is composite unique key to the parent
current_seg=${root_seg}
parent_seg=${root_seg}
declare -A SEGMENT_ID
SEGMENT_ID[${root_seg}]=1
for key in ${!CONFIG_ARY[*]}
do
  SEGMENT_ID[${key}]=0
done

while read line
do
  seg=`echo ${line} | cut -c ${keylen}`
  if [ ${current_seg} != ${seg} ]; then
    pseg=${CONFIG_ARY[${seg}]}
    
    if [ ${current_seg} = ${pseg} ]; then
      # move to child segment
      parent_seg=${pseg}
      current_seg=${seg}
    elif [ ${parent_seg} = ${pseg} ]; then
      # move to sibling segment
      current_seg=${seg}
    else
      # possibly move to ancestral segment
      # in that case, pre-current's ancestral segment equals current's parent segment
      anc_seg=${parent_seg}
      while [ ${anc_seg} != ${pseg} ]
      do
        if [ ${anc_seg} = ${root_seg} ]; then
          echo "INVALID segment: pre-current=${current_seg}, pre-parent=${parent_seg}, current=${seg}, parent=${pseg}"
          exit -1
        fi
        anc_seg=${CONFIG_ARY[${anc_seg}]}
      done
      
      # move to ancestral segment
      parent_seg=${pseg}
      current_seg=${seg}
    fi
  else
    # same kind segment; nothing to do
    echo "dummy" > /dev/null
  fi

  # update segment-id
  idx=${SEGMENT_ID[${current_seg}]}
  SEGMENT_ID[${current_seg}]=`expr ${idx} \+ 1`
  pidx=`expr ${SEGMENT_ID[${parent_seg}]} \- 1`

  # append record to current segment's file
  echo "${pidx}${parent_seg}${idx}${line}" >> ${ipath}/${iname}_${current_seg}
done < ${i_file}

